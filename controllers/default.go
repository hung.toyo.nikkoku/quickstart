package controllers

import (
	beego "github.com/beego/beego/v2/server/web"
)

type MainController struct {
	beego.Controller
}

func (c *MainController) Get() {
	c.Data["Website"] = "hungpd9.com"
	c.Data["Email"] = "hung.toyo.nikkoku.vn"
	c.TplName = "index.tpl"
}
